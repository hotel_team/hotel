/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.dao;

import itu.hotel.annotation.TableName;
import itu.hotel.model.BaseModel;
import itu.hotel.util.ConnexionDB;
import itu.hotel.util.Criteria;
import itu.hotel.util.Fonction;
import itu.hotel.util.Query;
import itu.hotel.util.WeakConcurrentHashMap;
import java.lang.reflect.Field;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manoa
 */
public class GeneriqueDAO implements InterfaceDAO{

    private WeakConcurrentHashMap weakConcurrentHashMap;
    
    public GeneriqueDAO(WeakConcurrentHashMap weakConcurrentHashMap){
        this.weakConcurrentHashMap = weakConcurrentHashMap;
    }
    
    @Override
    public void save(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        Fonction fonction = new Fonction();
        try{
            Class classTable = baseModel.getClass();
            String tableName = fonction.getNomTable(classTable);
            
            ArrayList<Field> column = new ArrayList<Field>();
            column = fonction.getAllFieldInBD(classTable, column, 1);
            
            String query = fonction.getQuery(tableName, column);
            
            ArrayList<String> information = fonction.getMethodClass(baseModel, column);
            
            // Connexion
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            for(int i=0;i<information.size();i++){
                Method method = classTable.getMethod(information.get(i));
                if(method.invoke(baseModel) == null){
                    preparedStatement.setObject(i+1,null);
                }
                else{
                    if(column.get(i).getType().toString().equals("class java.util.Date")){
                        Date dateUtil = (Date)method.invoke(baseModel);
                        preparedStatement.setDate(i+1, new java.sql.Date(dateUtil.getTime()));
                    }
                    else{
                        preparedStatement.setObject(i+1,method.invoke(baseModel)+"");
                    }
                }
            }
            preparedStatement.executeUpdate();
        }
        catch(Exception e){
            preparedStatement.cancel();
            throw e;
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect!= null){
                connect.close();
            }
        }
    }

    @Override
    public List<BaseModel> findAll(BaseModel baseModel) throws Exception {
        List<BaseModel> result = new ArrayList();
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Fonction fonction = new Fonction();
        try
        {
            Class classTable = baseModel.getClass();
            String tableName = fonction.getNomTable(classTable);
            
            //Check Cache
            Boolean checkCache = weakConcurrentHashMap.checkIfExist(tableName+"All");
            if(checkCache){
                result = (List<BaseModel>)weakConcurrentHashMap.getExist(tableName+"All");
            }
            else{
                String query = ("select*from "+tableName);
            
                ArrayList<Field> column = new ArrayList<Field>();
                column = fonction.getAllFieldInBD(classTable, column, 0);

                Object[] data = null;
                Class[] typeAttribut = null;

                connect = con.connectSql();
                preparedStatement = connect.prepareStatement(query);
                rs = preparedStatement.executeQuery();
                while (rs.next())
                {
                    data = new Object[column.size()];
                    typeAttribut = new Class[column.size()];
                    for(int i=0;i<column.size();i++){
                        typeAttribut[i] = column.get(i).getType();
                        data[i] = rs.getObject(column.get(i).getName()+"");
                    }
                    Constructor constructeur = classTable.getConstructor(typeAttribut);
                    BaseModel object = (BaseModel)constructeur.newInstance(data);
                    result.add(object);
                }
                weakConcurrentHashMap.createNew(tableName+"All", result);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
        return result;
    }

    @Override
    public BaseModel findById(BaseModel baseModel) throws Exception {
        BaseModel result = null;
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Fonction fonction = new Fonction();
        try
        {
            Class classTable = baseModel.getClass();
            result = (BaseModel)classTable.newInstance();
            String tableName = fonction.getNomTable(classTable);
            //Check Cache
            Boolean checkCache = weakConcurrentHashMap.checkIfExist(tableName+"Id");
            if(checkCache){
                result = (BaseModel)weakConcurrentHashMap.getExist(tableName+"Id");
            }
            else{
                String query = "select*from "+tableName+" where id = ?";
            
                ArrayList<Field> column = new ArrayList<Field>();
                column = fonction.getAllFieldInBD(classTable, column, 0);

                Object[] data = null;
                Class[] typeAttribut = null;
                connect = con.connectSql();
                preparedStatement = connect.prepareStatement(query);
                preparedStatement.setObject(1,baseModel.getId());
                rs = preparedStatement.executeQuery();
                while (rs.next())
                {
                    data = new Object[column.size()];
                    typeAttribut = new Class[column.size()];
                    for(int i=0;i<column.size();i++){
                        typeAttribut[i] = column.get(i).getType();
                        data[i] = rs.getObject(column.get(i).getName()+"");
                    }
                    Constructor constructeur = classTable.getConstructor(typeAttribut);
                    result = (BaseModel)constructeur.newInstance(data);
                }
                weakConcurrentHashMap.createNew(tableName+"Id", result);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
        return result;
    }

    @Override
    public void delete(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        Fonction fonction = new Fonction();
        try
        {
            Class classTable = baseModel.getClass();
            String tableName = fonction.getNomTable(classTable);
            String query = "delete from "+tableName+" where id=?";
            
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setObject(1,baseModel.getId());
            preparedStatement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
    }

    @Override
    public void update(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        Fonction fonction = new Fonction();
        try{
            Class classTable = baseModel.getClass();
            String tableName = fonction.getNomTable(classTable);
            
            ArrayList<Field> column = new ArrayList<Field>();
            column = fonction.getAllFieldInBD(classTable, column, 1);
            
            String query = fonction.getQueryUpdate(tableName, column);
            ArrayList<String> information = fonction.getMethodClass(baseModel, column);
            
            // Connexion
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            int indice = 1;
            for(int i=0;i<information.size();i++){
                Method method = classTable.getMethod(information.get(i));
                if(method.invoke(baseModel) == null){
                    preparedStatement.setObject(i+1,null);
                }
                else{
                    if(column.get(i).getType().toString().equals("class java.util.Date")){
                        Date dateUtil = (Date)method.invoke(baseModel);
                        preparedStatement.setDate(i+1, new java.sql.Date(dateUtil.getTime()));
                    }
                    else{
                        preparedStatement.setObject(i+1,method.invoke(baseModel)+"");
                    }
                }
                indice++;
            }
            preparedStatement.setObject(indice,baseModel.getId());
            preparedStatement.executeUpdate();
        }
        catch(Exception e){
            preparedStatement.cancel();
            throw e;
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect!= null){
                connect.close();
            }
        }
    }

    @Override
    public List<BaseModel> find(Query query, Class c) throws Exception {
        List<BaseModel> result = new ArrayList();
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Fonction fonction = new Fonction();
        try
        {
            Criteria criteria = query.getCriteria();
            String tableName = fonction.getNomTable(c);
            String request = fonction.getQueryCriteria(tableName, criteria);
            System.out.println(request);
            
            ArrayList<Field> column = new ArrayList<Field>();
            column = fonction.getAllFieldInBD(c, column, 0);
            
            Object[] data = null;
            Class[] typeAttribut = null;
            
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(request);
            List<String> listData = criteria.getListData();
            for(int i=0;i<listData.size();i++){
                preparedStatement.setObject(i+1,listData.get(i));
            }
            rs = preparedStatement.executeQuery();
            
            while (rs.next())
            {
                data = new Object[column.size()];
                typeAttribut = new Class[column.size()];
                for(int i=0;i<column.size();i++){
                    typeAttribut[i] = column.get(i).getType();
                    data[i] = rs.getObject(column.get(i).getName()+"");
                }
                Constructor constructeur = c.getConstructor(typeAttribut);
                BaseModel object = (BaseModel)constructeur.newInstance(data);
                result.add(object);
            }
        }
        catch (Exception e)
        {
            throw new Exception("Erreur lors de la fonction multicritere, verifier si les champs de table choisis sont bien correspondants");
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
        return result;
    }
    
}
