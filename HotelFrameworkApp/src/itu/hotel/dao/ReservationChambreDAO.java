/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.dao;

import itu.hotel.model.BaseModel;
import itu.hotel.model.ReservationChambre;
import itu.hotel.util.ConnexionDB;
import itu.hotel.util.Query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manoa
 */
public class ReservationChambreDAO implements InterfaceDAO{

    @Override
    public void save(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        try
        {
            ReservationChambre reservationChambre = (ReservationChambre)baseModel;
            String query = "insert into reservationChambre(idClient, dateDebut, dateFin, dateValidation, idChambre, nombrePersonne) values(?, ?, ?, ?, ?, ?)";
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, reservationChambre.getIdClient());
            
            if(reservationChambre.getDateDebut() == null){
                preparedStatement.setString(2, null);
            }
            else{
                preparedStatement.setDate(2, new java.sql.Date(reservationChambre.getDateDebut().getTime()));
            }
            
            if(reservationChambre.getDateFin() == null){
                preparedStatement.setString(3, null);
            }
            else{
                preparedStatement.setDate(3, new java.sql.Date(reservationChambre.getDateFin().getTime()));
            }
            
            if(reservationChambre.getDateValidation() == null){
                preparedStatement.setString(4, null);
            }
            else{
                preparedStatement.setDate(4, new java.sql.Date(reservationChambre.getDateValidation().getTime()));
            }
            
            preparedStatement.setInt(5, reservationChambre.getIdChambre());
            preparedStatement.setInt(6, reservationChambre.getNombrePersonne());
            preparedStatement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
    }

    @Override
    public List<BaseModel> findAll(BaseModel baseModel) throws Exception {
        List<BaseModel> result = new ArrayList();
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try
        {
            String query = ("select*from reservationChambre");
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next())
            {
                int id = rs.getInt("id");
                int idClient = rs.getInt("idClient");
                Date dateDebut = rs.getDate("dateDebut");
                Date dateFin = rs.getDate("dateFin");
                Date dateValidation = rs.getDate("dateValidation");
                int idChambre = rs.getInt("idChambre");
                int nombrePersonne = rs.getInt("nombrePersonne");
                result.add(new ReservationChambre(id, idClient, dateDebut, dateFin, dateValidation, idChambre, nombrePersonne));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(connect != null){
                connect.close();
            }
        }
        return result;
    }

    @Override
    public BaseModel findById(BaseModel baseModel) throws Exception {
        ReservationChambre result = null;
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try
        {
            String query = ("select*from reservationChambre where id = ?");
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setObject(1,baseModel.getId());
            rs = preparedStatement.executeQuery();
            while (rs.next())
            {
                int id = rs.getInt("id");
                int idClient = rs.getInt("idClient");
                Date dateDebut = rs.getDate("dateDebut");
                Date dateFin = rs.getDate("dateFin");
                Date dateValidation = rs.getDate("dateValidation");
                int idChambre = rs.getInt("idChambre");
                int nombrePersonne = rs.getInt("nombrePersonne");
                result = new ReservationChambre(id, idClient, dateDebut, dateFin, dateValidation, idChambre, nombrePersonne);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(connect != null){
                connect.close();
            }
        }
        return result;
    }

    @Override
    public void update(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        try
        {
            ReservationChambre reservationChambre = (ReservationChambre)baseModel;
            String query = "update reservationChambre set idClient=?, dateDebut=?, dateFin=?, dateValidation=?, idChambre=?, nombrePersonne=? where id=?";
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, reservationChambre.getIdClient());
            
            if(reservationChambre.getDateDebut() == null){
                preparedStatement.setString(2, null);
            }
            else{
                preparedStatement.setDate(2, new java.sql.Date(reservationChambre.getDateDebut().getTime()));
            }
            
            if(reservationChambre.getDateFin() == null){
                preparedStatement.setString(3, null);
            }
            else{
                preparedStatement.setDate(3, new java.sql.Date(reservationChambre.getDateFin().getTime()));
            }
            
            if(reservationChambre.getDateValidation() == null){
                preparedStatement.setString(4, null);
            }
            else{
                preparedStatement.setDate(4, new java.sql.Date(reservationChambre.getDateValidation().getTime()));
            }
            
            preparedStatement.setInt(5, reservationChambre.getIdChambre());
            preparedStatement.setInt(6, reservationChambre.getNombrePersonne());
            preparedStatement.setInt(7, reservationChambre.getId());
            preparedStatement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
    }

    @Override
    public void delete(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        try
        {
            String query = "delete from reservationChambre where id=?";
            
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setObject(1,baseModel.getId());
            preparedStatement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
    }

    @Override
    public List<BaseModel> find(Query query, Class c) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
