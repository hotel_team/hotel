/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.dao;

import itu.hotel.model.BaseModel;
import itu.hotel.util.Query;
import java.util.List;

/**
 *
 * @author Manoa
 */
public interface InterfaceDAO {
    public void save(BaseModel baseModel) throws Exception;
    public List<BaseModel> findAll(BaseModel baseModel) throws Exception;
    public BaseModel findById(BaseModel baseModel) throws Exception;
    public void update(BaseModel baseModel) throws Exception;
    public void delete(BaseModel baseModel) throws Exception;
    public List<BaseModel> find(Query query, Class c) throws Exception;
}
