/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.dao;

import itu.hotel.model.BaseModel;
import itu.hotel.util.ConnexionDB;
import itu.hotel.util.Fonction;
import itu.hotel.util.WeakConcurrentHashMap;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Query;

/**
 *
 * @author Manoa
 */
public class GeneriqueHibernateDAO implements InterfaceDAO{
    
    private SessionFactory sessionFactory;
    private WeakConcurrentHashMap weakConcurrentHashMap;
    
    public GeneriqueHibernateDAO(SessionFactory sessionFactory, WeakConcurrentHashMap weakConcurrentHashMap){
        this.sessionFactory = sessionFactory;
        this.weakConcurrentHashMap = weakConcurrentHashMap;
    }

    @Override
    public void save(BaseModel baseModel) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.save(baseModel);
            tx.commit();
            System.out.println("Saved");
        }
        catch(Exception e){
            if (tx!=null) tx.rollback();
            throw e;
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<BaseModel> findAll(BaseModel baseModel) throws Exception {
        Session session = sessionFactory.openSession();
        Fonction fonction = new Fonction();
        try{
            Class classTable = baseModel.getClass();
            String tableName = fonction.getNomTable(classTable);
            //Check Cache
            Boolean checkCache = weakConcurrentHashMap.checkIfExist(tableName+"All");
            if(checkCache){
                return (List<BaseModel>)weakConcurrentHashMap.getExist(tableName+"All");
            }
            else{
                weakConcurrentHashMap.createNew(tableName+"All", session.createQuery("FROM "+tableName).list());
                return session.createQuery("FROM "+tableName).list();
            }
        }
        catch(Exception e){
            throw e;
        }
        finally{
            session.close();
        }
    }

    @Override
    public BaseModel findById(BaseModel baseModel) throws Exception {
        Session session = sessionFactory.openSession();
        Fonction fonction = new Fonction();
        try{
            Class classTable = baseModel.getClass();
            String tableName = fonction.getNomTable(classTable);
            //Check Cache
            Boolean checkCache = weakConcurrentHashMap.checkIfExist(tableName+"Id");
            if(checkCache){
                return (BaseModel)weakConcurrentHashMap.getExist(tableName+"Id");
            }
            else{
                String queryString = "from "+tableName+" where id = :id";
                //Query query = (Query) session.createQuery(queryString);
                Query query = session.createQuery(queryString);
                query.setInteger("id", baseModel.getId());
                if(query.uniqueResult() == null){
                    throw new Exception("No data found");
                }
                weakConcurrentHashMap.createNew(tableName+"Id", query.uniqueResult());
                return (BaseModel) query.uniqueResult();
            }
            
        }
        catch(Exception e){
            throw e;
        }
        finally{
            session.close();
        }
    }

    @Override
    public void update(BaseModel baseModel) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        Fonction fonction = new Fonction();
        try{
            Class classTable = baseModel.getClass();
            tx = session.beginTransaction();
            
            ArrayList<Field> column = new ArrayList<Field>();
            column = fonction.getAllFieldInBD(classTable, column, 1);
            ArrayList<String> information = fonction.getMethodClass(baseModel, column);
            ArrayList<String> informationSet = fonction.getMethodClassSet(baseModel, column);
            Object baseModelGet = (BaseModel)session.get(classTable, baseModel.getId());
            if(baseModelGet == null){
                throw new Exception("No data found");
            }
            else{
                // Not working
                // Not Working
                // NOT WORKING
                // Work if it's not a date
                for(int i=0;i<information.size();i++){
                    Method method = classTable.getMethod(information.get(i));
                    Method method2 = classTable.getMethod(informationSet.get(i), column.get(i).getType());
                    if(method.invoke(baseModel) == null){
                        method.invoke(baseModelGet, null);
                    }
                    else{
                        if(column.get(i).getType().toString().equals("class java.util.Date")){
                            Date dateUtil = (Date)method.invoke(baseModel);
                            System.out.println((dateUtil.getYear()+1900)+","+(dateUtil.getMonth()+1)+","+dateUtil.getDate());
                            method2.invoke(baseModelGet, new java.sql.Date((dateUtil.getYear()+1900),(dateUtil.getMonth()+1),dateUtil.getDate()));
                        }
                        else{
                            method2.invoke(baseModelGet, method.invoke(baseModel));
                        }
                    }
                }
                session.update(baseModelGet); 
                tx.commit();
                System.out.println("Updated");
            }
        }
        catch(Exception e){
            if (tx!=null) tx.rollback();
            e.printStackTrace();
            throw e;
        }
        finally{
            session.close();
        }
    }

    @Override
    public void delete(BaseModel baseModel) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try{
            Class classTable = baseModel.getClass();
            tx = session.beginTransaction();
            BaseModel baseModelGet = (BaseModel)session.get(classTable, baseModel.getId()); 
            if(baseModelGet == null){
                throw new Exception("No data found");
            }
            else{
                session.delete(baseModelGet); 
                tx.commit();
                System.out.println("Deleted");
            }
        }
        catch(Exception e){
            if (tx!=null) tx.rollback();
            throw e;
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<BaseModel> find(itu.hotel.util.Query query, Class c) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}
