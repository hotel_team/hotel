/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.dao;

import itu.hotel.model.BaseModel;
import itu.hotel.model.Cheking;
import itu.hotel.util.ConnexionDB;
import itu.hotel.util.Query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manoa
 */
public class ChekingDAO implements InterfaceDAO{

    @Override
    public void save(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        try
        {
            Cheking cheking = (Cheking)baseModel;
            String query = "insert into cheking(idClient, idReservation, dateArrive, dateDepart, etat) values(?, ?, ?, ?, ?)";
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, cheking.getIdClient());
            preparedStatement.setInt(2, cheking.getIdReservation());
            
            if(cheking.getDateArrive() == null){
                preparedStatement.setString(3, null);
            }
            else{
                preparedStatement.setDate(3, new java.sql.Date(cheking.getDateArrive().getTime()));
            }
            
            if(cheking.getDateDepart() == null){
                preparedStatement.setString(4, null);
            }
            else{
                preparedStatement.setDate(4, new java.sql.Date(cheking.getDateDepart().getTime()));
            }
            
            preparedStatement.setInt(5, cheking.getEtat());
            preparedStatement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
    }

    @Override
    public List<BaseModel> findAll(BaseModel baseModel) throws Exception {
        List<BaseModel> result = new ArrayList();
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try
        {
            String query = ("select*from cheking");
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            rs = preparedStatement.executeQuery();
            while (rs.next())
            {
                int id = rs.getInt("id");
                int idClient = rs.getInt("idClient");
                int idReservation = rs.getInt("idReservation");
                Date dateArrive = rs.getDate("dateArrive");
                Date dateDepart = rs.getDate("dateDepart");
                int etat = rs.getInt("etat");
                result.add(new Cheking(id, idClient, idReservation, dateArrive, dateDepart, etat));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(connect != null){
                connect.close();
            }
        }
        return result;
    }

    @Override
    public BaseModel findById(BaseModel baseModel) throws Exception {
        Cheking result = null;
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try
        {
            String query = ("select*from cheking where id = ?");
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setObject(1,baseModel.getId());
            rs = preparedStatement.executeQuery();
            while (rs.next())
            {
                int id = rs.getInt("id");
                int idClient = rs.getInt("idClient");
                int idReservation = rs.getInt("idReservation");
                Date dateArrive = rs.getDate("dateArrive");
                Date dateDepart = rs.getDate("dateDepart");
                int etat = rs.getInt("etat");
                result = new Cheking(id, idClient, idReservation, dateArrive, dateDepart, etat);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(rs != null){
                rs.close();
            }
            if(connect != null){
                connect.close();
            }
        }
        return result;
    }

    @Override
    public void update(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        try
        {
            Cheking cheking = (Cheking)baseModel;
            String query = "update cheking set idClient=?, idReservation=?, dateArrive=?, dateDepart=?, etat=? where id=?";
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setInt(1, cheking.getIdClient());
            preparedStatement.setInt(2, cheking.getIdReservation());
            
            if(cheking.getDateArrive() == null){
                preparedStatement.setString(3, null);
            }
            else{
                preparedStatement.setDate(3, new java.sql.Date(cheking.getDateArrive().getTime()));
            }
            
            if(cheking.getDateDepart() == null){
                preparedStatement.setString(4, null);
            }
            else{
                preparedStatement.setDate(4, new java.sql.Date(cheking.getDateDepart().getTime()));
            }
            
            preparedStatement.setInt(5, cheking.getEtat());
            preparedStatement.setInt(6, cheking.getId());
            preparedStatement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
    }

    @Override
    public void delete(BaseModel baseModel) throws Exception {
        ConnexionDB con = new ConnexionDB();
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        try
        {
            String query = "delete from cheking where id=?";
            
            connect = con.connectSql();
            preparedStatement = connect.prepareStatement(query);
            preparedStatement.setObject(1,baseModel.getId());
            preparedStatement.executeUpdate();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if(preparedStatement != null){
                preparedStatement.close();
            }
            if(connect != null){
                connect.close();
            }
        }
    }

    @Override
    public List<BaseModel> find(Query query, Class c) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
