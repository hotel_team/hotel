/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.model;

import itu.hotel.annotation.NotInDB;
import itu.hotel.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Manoa
 */
@Entity
@TableName(table = "ReservationChambre")
@Table(name = "ReservationChambre")
//@Access(value=AccessType.FIELD)
public class ReservationChambre extends BaseModel implements Serializable{
    @Id
    @Column(name="id")
    @GeneratedValue
    @NotInDB
    private int id;
    
    @Column(name = "idClient")
    private int idClient;
    
    @Column(name = "dateDebut")
    private Date dateDebut;
    
    @Column(name = "dateFin")
    private Date dateFin;
    
    @Column(name = "dateValidation")
    private Date dateValidation;
    
    @Column(name = "idChambre")
    private int idChambre;
    
    @Column(name = "nombrePersonne")
    private int nombrePersonne;
    
    @Transient
    @NotInDB
    private String notInDB;
    
    @Transient
    @NotInDB
    private String notInDB2;

    public ReservationChambre() {
    }

    public ReservationChambre(int id, int idClient, Date dateDebut, Date dateFin, Date dateValidation, int idChambre, int nombrePersonne) throws Exception{
        super.setId(id);
        this.setIdClient(idClient);
        this.setDateDebut(dateDebut);
        this.setDateFin(dateFin);
        this.setDateValidation(dateValidation);
        this.setIdChambre(idChambre);
        this.setNombrePersonne(nombrePersonne);
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }
    
    public Date getDateDebut()
    {
        if(this.dateDebut == null){
            return null;
        }
        else{
            return new Date(this.dateDebut.getYear(), this.dateDebut.getMonth(), this.dateDebut.getDate());
        }
    }
    public void setDateDebut(Date date) throws Exception
    {
        if(date == null){
            this.dateDebut = null;
        }
        else{
            date = new Date(date.getYear()-1900, date.getMonth()-1, date.getDate());
            this.dateDebut = date;
        }
    }
    
    /*public Date getDateDebut()
    {
        if(this.dateDebut == null){
            return null;
        }
        else{
            return new Date(this.dateDebut.getYear()-1900, this.dateDebut.getMonth()-1, this.dateDebut.getDate());
        }
    }
    public void setDateDebut(Date date) throws Exception
    {
        if(date == null){
            this.dateDebut = null;
        }
        else{
            date = new Date(date.getYear()+1900, date.getMonth()+1, date.getDate());
            this.dateDebut = date;
        }
    }*/
    
    public Date getDateFin()
    {
        if(this.dateFin == null){
            return null;
        }
        else{
            return new Date(this.dateFin.getYear(), this.dateFin.getMonth(), this.dateFin.getDate());
        }
    }
    public void setDateFin(Date date) throws Exception
    {
        if(date == null){
            this.dateFin = null;
        }
        else{
            date = new Date(date.getYear()-1900, date.getMonth()-1, date.getDate());
            this.dateFin = date;
        }
    }
    
    /*public Date getDateFin()
    {
        if(this.dateFin == null){
            return null;
        }
        else{
            return new Date(this.dateFin.getYear()-1900, this.dateFin.getMonth()-1, this.dateFin.getDate());
        }
    }
    public void setDateFin(Date date) throws Exception
    {
        if(date == null){
            this.dateFin = null;
        }
        else{
            date = new Date(date.getYear()+1900, date.getMonth()+1, date.getDate());
            this.dateFin = date;
        }
    }*/
    
    public Date getDateValidation()
    {
        if(this.dateValidation == null){
            return null;
        }
        else{
            return new Date(this.dateValidation.getYear(), this.dateValidation.getMonth(), this.dateValidation.getDate());
        }
    }
    public void setDateValidation(Date date) throws Exception
    {
        if(date == null){
            this.dateValidation = null;
        }
        else{
            date = new Date(date.getYear()-1900, date.getMonth()-1, date.getDate());
            this.dateValidation = date;
        }
    }
    
    /*public Date getDateValidation()
    {
        if(this.dateValidation == null){
            return null;
        }
        else{
            return new Date(this.dateValidation.getYear()-1900, this.dateValidation.getMonth()-1, this.dateValidation.getDate());
        }
    }
    public void setDateValidation(Date date) throws Exception
    {
        if(date == null){
            this.dateValidation = null;
        }
        else{
            date = new Date(date.getYear()+1900, date.getMonth()+1, date.getDate());
            this.dateValidation = date;
        }
    }*/

    public int getIdChambre() {
        return idChambre;
    }

    public void setIdChambre(int idChambre) {
        this.idChambre = idChambre;
    }

    public int getNombrePersonne() {
        return nombrePersonne;
    }

    public void setNombrePersonne(int nombrePersonne) {
        this.nombrePersonne = nombrePersonne;
    }
    
    
}
