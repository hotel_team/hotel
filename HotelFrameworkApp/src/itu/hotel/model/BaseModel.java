/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.model;

import itu.hotel.annotation.NotInDB;
import itu.hotel.annotation.Sequential;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Manoa
 */
public class BaseModel {
    
    @Sequential
    @Id
    @Column(name="id")
    @GeneratedValue
    protected int id;
    
    @NotInDB
    private String nomTable;

    public BaseModel() {
    }

    public BaseModel(int id, String nomTable) {
        this.id = id;
        this.nomTable = nomTable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomTable() {
        return nomTable;
    }

    public void setNomTable(String nomTable) {
        this.nomTable = nomTable;
    }
}
