/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.model;

import itu.hotel.annotation.NotInDB;
import itu.hotel.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Manoa
 */
@Entity
@TableName(table = "Cheking")
@Table(name = "cheking")
public class Cheking extends BaseModel implements Serializable{
    @Id
    @Column(name="id")
    @GeneratedValue
    @NotInDB
    private int id;
    
    @Column(name = "idClient")
    private int idClient;
    
    @Column(name = "idReservation")
    private int idReservation;
    
    @Column(name = "dateArrive")
    private Date dateArrive;
    
    @Column(name = "dateDepart")
    private Date dateDepart;
    
    @Column(name = "etat")
    private int etat;
    
    @Transient
    @NotInDB
    private String notInDB;
    
    @Transient
    @NotInDB
    private String notInDB2;
    
    public Cheking(int id,int idClient,int idReservation,Date dateArrive,Date dateDepart,int etat)throws Exception{
        super.setId(id);
        this.setIdClient(idClient);
        this.setIdReservation(idReservation);
        this.setDateArrive(dateArrive);
        this.setDateDepart(dateDepart);
        this.setEtat(etat);
    }
    
    public int getIdClient(){
        return idClient;
    }
    
    public int getIdReservation(){
        return idReservation;
    }
    
    // Hibernate
    public Date getDateArrive()
    {
        if(this.dateArrive == null){
            return null;
        }
        else{
            return new Date(this.dateArrive.getYear(), this.dateArrive.getMonth(), this.dateArrive.getDate());
        }
    }
    
    /*public Date getDateArrive()
    {
        if(this.dateArrive == null){
            return null;
        }
        else{
            return new Date(this.dateArrive.getYear()-1900, this.dateArrive.getMonth()-1, this.dateArrive.getDate());
        }
    }*/
    
    public Date getDateDepart()
    {
        if(this.dateDepart == null){
            return null;
        }
        else{
            return new Date(this.dateDepart.getYear(), this.dateDepart.getMonth(), this.dateDepart.getDate());
        }
    }
    
    /*public Date getDateDepart()
    {
        if(this.dateDepart == null){
            return null;
        }
        else{
            return new Date(this.dateDepart.getYear()-1900, this.dateDepart.getMonth()-1, this.dateDepart.getDate());
        }
    }*/
    
    public int getEtat(){
        return etat;
    }
    
    public void setIdClient(int idClient){
        this.idClient = idClient;
    }
    
    public void setIdReservation(int idReservation){
        this.idReservation = idReservation;
    }
    
    // Hibernate
    public void setDateArrive(Date date) throws Exception{
        if(date == null){
            this.dateArrive = null;
        }
        else{
            date = new Date(date.getYear()-1900, date.getMonth()-1, date.getDate());
            this.dateArrive = date;
        }
    }
    
    /*public void setDateArrive(Date date) throws Exception
    {
        if(date == null){
            this.dateArrive = null;
        }
        else{
            date = new Date(date.getYear()+1900, date.getMonth()+1, date.getDate());
            this.dateArrive = date;
        }
    }*/
    
    public void setDateDepart(Date date) throws Exception{
        if(date == null){
            this.dateDepart = null;
        }
        else{
            date = new Date(date.getYear()-1900, date.getMonth()-1, date.getDate());
            this.dateDepart = date;
        }
    }
    
    /*public void setDateDepart(Date date) throws Exception
    {
        if(date == null){
            this.dateDepart = null;
        }
        else{
            date = new Date(date.getYear()+1900, date.getMonth()+1, date.getDate());
            this.dateDepart = date;
        }
    }*/
    
    public void setEtat(int etat){
        this.etat = etat;
    }
    
    public Cheking(){   
    }
}
