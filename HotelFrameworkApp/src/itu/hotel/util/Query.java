/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.util;

/**
 *
 * @author Manoa
 */
public class Query {
    private Criteria criteria;

    public Query() {
    }

    public Query(Criteria criteria) {
        this.setCriteria(criteria);
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }
}
