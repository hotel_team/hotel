/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import itu.hotel.annotation.NotInDB;
import itu.hotel.annotation.Sequential;
import itu.hotel.annotation.TableName;
import itu.hotel.model.BaseModel;
import java.lang.reflect.Method;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author Manoa
 */
public class Fonction {
    public ArrayList<Field> getAllFieldInBD(Class table, ArrayList<Field> field, int indice){
        Field[] column = table.getDeclaredFields();
         // indice = 1 => insert , indice = 0 => find
        if(indice == 1){
            for(int i=0;i<column.length;i++){
                if(!column[i].isAnnotationPresent(NotInDB.class) && !column[i].isAnnotationPresent(Sequential.class)){
                    field.add(column[i]);
                }
            }
        }
        else{
            for(int i=0;i<column.length;i++){
                if(!column[i].isAnnotationPresent(NotInDB.class)){
                    if(column[i].isAnnotationPresent(Sequential.class)){
                        field.add(0, column[i]);
                    }
                    else{
                        field.add(column[i]);
                    }
                }
            }
        }
        
        table = table.getSuperclass();
        if(!table.getSimpleName().equals("Object")){
            getAllFieldInBD(table, field, indice);
        }
        return field;
    }
    
    public ArrayList<String> getMethodClass(BaseModel baseModel, ArrayList<Field> column)throws Exception{
        ArrayList<String> result = new ArrayList<String>();
        try{
            for(int i=0;i<column.size();i++){
                String field = column.get(i).getName();
                field = "get"+field.substring(0,1).toUpperCase()+field.substring(1);
                result.add(field);
            }
            return result;
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public ArrayList<String> getMethodClassSet(BaseModel baseModel, ArrayList<Field> column)throws Exception{
        ArrayList<String> result = new ArrayList<String>();
        try{
            for(int i=0;i<column.size();i++){
                String field = column.get(i).getName();
                field = "set"+field.substring(0,1).toUpperCase()+field.substring(1);
                result.add(field);
            }
            return result;
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public String getNomTable(Class classTable){
        TableName annotation = (TableName) classTable.getAnnotation(TableName.class);
        return annotation.table();
    }
    
    public String getQuery(String tableName, ArrayList<Field> column){
        String beginSql = "insert into "+tableName+" ";
        String sql = "(";
        String values = "(";
        for(int i=0;i<column.size();i++){
            sql = sql+""+column.get(i).getName()+",";
            values = values+"?,";
        }

        sql = sql.substring(0,sql.length()-1);
        values = values.substring(0,values.length()-1);
        sql = sql+")";
        values = values+")";

        return beginSql+sql+" values "+values;
    }
    
    public String getQueryUpdate(String tableName, ArrayList<Field> column){
        String beginSql = "update "+tableName+" set ";
        String sql = "";
        for(int i=0;i<column.size();i++){
            sql = sql+""+column.get(i).getName()+"=?, ";
        }
        sql = sql.substring(0,sql.length()-2);
        sql = beginSql+sql+" where id=?";
        return sql;
    }
    
    public String getQueryCriteria(String tableName, Criteria criteria) throws Exception{
        String sql = "";
        try{
            String beginSql = "select * from "+tableName+" where 1<2 ";
            for(int i=0;i<criteria.getListColumn().size();i++){
                sql = sql+criteria.getListOperation().get(i)+" "+criteria.getListColumn().get(i)+" "+criteria.getListMark().get(i)+" ? ";
            }
            sql = beginSql+sql;
        }
        catch(Exception e){
            throw e;
        }
        return sql;
    }
    
    public SessionFactory prepareSessionFactory(){
        return new AnnotationConfiguration().
        configure("itu/hotel/util/hibernate.cfg.xml" ).
        buildSessionFactory();
    }
}
