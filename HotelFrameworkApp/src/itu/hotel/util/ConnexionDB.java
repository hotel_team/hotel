/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Manoa
 */
public class ConnexionDB {
    Connection conex;
    
    public Connection connectSql() throws Exception {
        try {
            //Chaine de connexion
            String url = "jdbc:sqlserver://localhost:1433;databaseName=hotel";
            String user = "sa";
            String pass = "root";
            //chargement du pilote sql server
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            /*Etablissement de la connection*/
            conex = DriverManager.getConnection(url+";user="+user+";password="+pass);
            //System.out.println("Connect to database successfuly");
            //JOptionPane.showMessageDialog(null, "connection a la base de donnees reussie");
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return conex;
    }
}
