/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manoa
 */
public class Criteria {
    private List<String> listColumn;
    private List<String> listMark;
    private List<String> listData;
    private List<String> listOperation;

    public Criteria() {
        initialize();
    }

    public Criteria(List<String> listColumn, List<String> listMark, List<String> listData, List<String> listOperation) {
        initialize();
        this.setListColomn(listColumn);
        this.setListMark(listMark);
        this.setListData(listData);
        this.setListOperation(listOperation);
    }

    public List<String> getListOperation() {
        return listOperation;
    }

    public void setListOperation(List<String> listOperation) {
        this.listOperation = listOperation;
    }
    
    private void initialize(){
        listColumn = new ArrayList();
        listMark = new ArrayList();
        listData = new ArrayList();
        listOperation = new ArrayList();
    }

    public List<String> getListColumn() {
        return listColumn;
    }

    public void setListColomn(List<String> listColumn) {
        this.listColumn = listColumn;
    }

    public List<String> getListMark() {
        return listMark;
    }

    public void setListMark(List<String> listMark) {
        this.listMark = listMark;
    }

    public List<String> getListData() {
        return listData;
    }

    public void setListData(List<String> listData) {
        this.listData = listData;
    }
    
    public static Criteria where(String column){
        Criteria criteria = new Criteria();
        if(criteria.listColumn.size() == 0){
            criteria.listOperation.add("and");
        }
        criteria.listColumn.add(column);
        return criteria;
    }
    
    public Criteria and(String column){
        this.listColumn.add(column);
        this.listOperation.add("and");
        return this;
    }
    
    public Criteria or(String column){
        this.listColumn.add(column);
        this.listOperation.add("or");
        return this;
    }
    
    public Criteria is(String data){
        this.listMark.add("=");
        this.listData.add(data);
        return this;
    }
    
    public Criteria lt(String data){
        this.listMark.add("<");
        this.listData.add(data);
        return this;
    }
    
    public Criteria gt(String data){
        this.listMark.add(">");
        this.listData.add(data);
        return this;
    }
    
    public Criteria like(String data){
        this.listMark.add("like");
        this.listData.add("%"+data+"%");
        return this;
    }
}
