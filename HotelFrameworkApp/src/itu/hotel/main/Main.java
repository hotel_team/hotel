/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itu.hotel.main;

import itu.hotel.dao.ChekingDAO;
import itu.hotel.dao.GeneriqueDAO;
import itu.hotel.dao.GeneriqueHibernateDAO;
import itu.hotel.dao.InterfaceDAO;
import itu.hotel.dao.ReservationChambreDAO;
import itu.hotel.model.BaseModel;
import itu.hotel.model.Cheking;
import itu.hotel.model.ReservationChambre;
import itu.hotel.util.Criteria;
import itu.hotel.util.Fonction;
import itu.hotel.util.InMemoryCache;
import itu.hotel.util.Query;
import itu.hotel.util.WeakConcurrentHashMap;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Manoa
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        try{
            WeakConcurrentHashMap weakConcurrentHashMap = new WeakConcurrentHashMap();
            //InterfaceDAO interfaceDAO = new ReservationChambreDAO();
            //InterfaceDAO interfaceDAO = new ChekingDAO();
            //InterfaceDAO interfaceDAO = new GeneriqueDAO(weakConcurrentHashMap);
            
            ReservationChambre reservationChambre = new ReservationChambre(1, 2, new Date(2020,9,20), new Date(2021,11,21), null, 1, 3);
            Cheking cheking = new Cheking(1,1,1,new Date(2020,12,01),new Date(2019,12,20),0);
            //System.out.println(reservationChambre.getDateDebut()+"/"+ reservationChambre.getDateValidation());
            /* SAVE */
            //interfaceDAO.save(reservationChambre);
            //interfaceDAO.save(cheking);

            /* FINDALL */
            /*List<BaseModel> bm = interfaceDAO.findAll(reservationChambre);
            for(BaseModel baseModel : bm){
                ReservationChambre st = (ReservationChambre)baseModel;
                System.out.println(st.getDateDebut()+" / "+st.getDateFin() +" / "+st.getDateValidation()+" / "+st.getIdClient());
            }
            List<BaseModel> bm2 = interfaceDAO.findAll(reservationChambre);
            for(BaseModel baseModel : bm2){
                ReservationChambre st = (ReservationChambre)baseModel;
                System.out.println(st.getDateDebut()+" / "+st.getDateFin() +" / "+st.getDateValidation()+" / "+st.getIdClient());
            }*/
            /*List<BaseModel> bm = interfaceDAO.findAll(cheking);
            for(BaseModel baseModel : bm){
                Cheking ck = (Cheking)baseModel;
                System.out.println(ck.getDateArrive()+" / "+ck.getDateDepart()+" / "+ck.getIdClient());
            }*/
            
            /* FINDBYID */
            //ReservationChambre reservationChambreFind = (ReservationChambre)interfaceDAO.findById(reservationChambre);
            //ReservationChambre reservationChambreFind2 = (ReservationChambre)interfaceDAO.findById(reservationChambre);
            //System.out.println(reservationChambreFind.getDateValidation() +" / "+reservationChambreFind.getDateDebut()+" / "+reservationChambreFind.getDateFin()+" / "+reservationChambreFind.getIdClient());
            /*Cheking ck = (Cheking)interfaceDAO.findById(cheking);
            System.out.println(ck.getDateArrive() +" / "+ck.getDateDepart()+" / "+ck.getIdClient());*/
            
            /* DELETE */
            //interfaceDAO.delete(reservationChambre);
            //interfaceDAO.delete(cheking);
            
            /* UPDATE */
            //interfaceDAO.update(reservationChambre);
            //interfaceDAO.update(cheking);
            
            /* MULTICRITERE */
            /*Criteria criteria = new Criteria();
            Query query = new Query(Criteria.where("nombrePersonne").like("5").or("nombrePersonne").is("2"));
            List<BaseModel> bm = interfaceDAO.find(query, ReservationChambre.class);
            for(BaseModel baseModel : bm){
                ReservationChambre st = (ReservationChambre)baseModel;
                System.out.println(st.getDateDebut()+" / "+st.getDateFin() +" / "+st.getDateValidation()+" / "+st.getIdClient());
            }*/
            /*Query query = new Query(Criteria.where("etat").is("0"));
            List<BaseModel> bm = interfaceDAO.find(query, Cheking.class);
            for(BaseModel baseModel : bm){
                Cheking ck = (Cheking)baseModel;
                System.out.println(ck.getDateArrive()+" / "+ck.getDateDepart() +" / "+ck.getIdClient());
            }*/
            
            
            // HIBERNATE
            
            /*Fonction fonction = new Fonction();
            SessionFactory sessionFactory = fonction.prepareSessionFactory();
            
            InterfaceDAO interfaceDAO = new GeneriqueHibernateDAO(sessionFactory, weakConcurrentHashMap);*/
            
            /*Session session = sessionFactory.openSession();
            Transaction tx = null;
            tx = session.beginTransaction();*/
            
            // Insert
            //interfaceDAO.save(reservationChambre);
            //interfaceDAO.save(cheking);
            
            // Delete
            //interfaceDAO.delete(reservationChambre);
            //interfaceDAO.delete(cheking);
            
            // Update
            //interfaceDAO.update(reservationChambre);
            //interfaceDAO.update(cheking);
            
            // FindAll
            /*List<BaseModel> bm = interfaceDAO.findAll(reservationChambre);
            for(BaseModel baseModel : bm){
                ReservationChambre reservation = (ReservationChambre)baseModel;
                System.out.println(reservation.getDateDebut()+" / "+reservation.getDateFin() +" / "+reservation.getDateValidation()+" / "+reservation.getIdClient());
            }
            List<BaseModel> bm2 = interfaceDAO.findAll(reservationChambre);
            for(BaseModel baseModel : bm2){
                ReservationChambre reservation = (ReservationChambre)baseModel;
                System.out.println(reservation.getDateDebut()+" / "+reservation.getDateFin() +" / "+reservation.getDateValidation()+" / "+reservation.getIdClient());
            }*/
            /*List<BaseModel> bm = interfaceDAO.findAll(cheking);
            for(BaseModel baseModel : bm){
                Cheking ck = (Cheking)baseModel;
                System.out.println(ck.getDateArrive()+" / "+ck.getDateDepart() +" / "+ck.getIdClient());
            }*/
            
            // FindById
            /*BaseModel bm3 = interfaceDAO.findById(reservationChambre);
            ReservationChambre reservation = (ReservationChambre)bm3;
            BaseModel bm4 = interfaceDAO.findById(reservationChambre);
            ReservationChambre reservation2 = (ReservationChambre)bm4;
            System.out.println(reservation.getDateDebut()+" / "+reservation.getDateFin() +" / "+reservation.getDateValidation()+" / "+reservation.getIdClient());*/
            /*BaseModel bm = interfaceDAO.findById(cheking);
            Cheking ck = (Cheking)bm;
            System.out.println(ck.getDateArrive()+" / "+ck.getDateDepart() +" / "+ck.getIdClient());*/
            
            /*ReservationChambre reservationChambreGet = (ReservationChambre)session.get(ReservationChambre.class, 55);
            if(reservationChambreGet == null){
                throw new Exception("No data found");
            }
            else{
                System.out.println(reservationChambreGet.getDateDebut()+" / "+reservationChambreGet.getDateFin() +" / "+reservationChambreGet.getDateValidation()+" / "+reservationChambreGet.getIdClient());
            }*/
        }
        catch (Exception e)
        {
            System.out.println("Exceptioooooonnnnnnn ! : "+e.getMessage());
        }
    }
    
}
